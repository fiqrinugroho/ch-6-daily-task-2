const router = require('express').Router()
// Routing
const Product = require('./products')
const Warehouse = require('./warehouses')
const Auth = require('./auth')

// endpoint warehouse
router.use('/api/v1/warehouses', Warehouse)
// endpoint products
router.use('/api/v1/products', Product)
// endpoint auth
router.use('/api/v1/auth', Auth)

module.exports = router
